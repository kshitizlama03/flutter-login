import 'package:shared_preferences/shared_preferences.dart';

addUserToSP(username, response) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(username, response);
}

getStringValuesSF(key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String data = prefs.getString(key) ?? null;
  return data; //string
}
